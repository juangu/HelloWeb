package com.amdocs.webapp;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWebApp extends HttpServlet {

	private static final long serialVersionUID = 1L;

        protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
	         throws ServletException, IOException {
            resp.setContentType("text/plain");
            resp.getWriter().write("Hello Madrid! Juan Speaking on 12/09/2019\n\n\nJava Project for DevOps hands-on Workshop in BriteBill, Spain.\n\n\n");
	    for(int i=0;i<10;i++)
		    resp.getWriter().write("MUCHO TRABAJAR Y POCO JUGAR HACEN DE JUAN UN ABURRIDO\n");
        }


        public int add(int a, int b) {
            return a + b;
        }

	public int sub(int a, int b) {
            return a - b;
        }

}
